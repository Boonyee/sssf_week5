var cacheName = 'hello-pwa';
var filesToCache = [
  '/',
  './index.html',
  './css/style.css',
  './js/main.js',
  './css/playfairdisplay-regular.woff',
  './css/playfairdisplay-regular.woff2',
  './css/acq_icon.svg',
  './css/background.jpg',
];

/* Start the service worker and cache all of the app's content */
self.addEventListener('install', (event) => {
  try {
    event.waitUntil(
      (async () => {
        try {
          const cache = await caches.open('v1');
          return cache.addAll(filesToCache);
        } catch (e) {
          console.log(e.message);
        }
      })()
    );
  } catch (error) {
    console.log(error);
  }
});

/* Serve cached content when offline */
self.addEventListener('fetch', (event) => {
  console.log('ServiceWorker Fetch', event.request.url);
  event.respondWith(
    (async () => {
      try {
        const response = await caches.match(event.request);
        return response || fetch(event.request);
      } catch (e) {
        console.log(e.message);
      }
    })()
  );
});
