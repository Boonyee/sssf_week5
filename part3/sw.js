'use strict';
self.importScripts('./js/fetchGQL.js');
self.importScripts('./js/idb.js');
const cacheName = 'grapqhl-animal';
const filesToCache = [
  './',
  './index.html',
  './favicon.ico',
  './css/style.css',
  './css/playfairdisplay-regular.woff',
  './css/playfairdisplay-regular.woff2',
  './images/background.jpg',
  './js/main.js',
  './js/idb.js',
];

/* Start the service worker and cache all of the app's content */
self.addEventListener('install', (e) => {
  e.waitUntil(
    (async () => {
      try {
        const cache = await caches.open(cacheName);
        // console.log(cache);
        return cache.addAll(filesToCache);
      } catch (e) {
        console.log('after install', e.message);
      }
    })()
  );
});

/* Serve cached content when offline */
self.addEventListener('fetch', (e) => {
  // console.log(e.request);
  e.respondWith(
    (async () => {
      try {
        const response = await caches.match(e.request);
        // console.log('resp', response);
        return response || fetch(e.request);
      } catch (e) {
        console.log('load cache', e.message);
      }
    })()
  );
});

self.addEventListener('sync', (event) => {
  if (event.tag === 'send-category') {
    event.waitUntil(sendCategoryToServer());
  }
  if (event.tag === 'send-species') {
    event.waitUntil(sendSpeciesToServer());
  }
  if (event.tag === 'send-animal') {
    event.waitUntil(sendAnimalToServer());
  }
});

const sendCategoryToServer = async () => {
  try {
    const outboxCat = await loadData('outboxCat');
    outboxCat.map((category) => {
      saveCategory(category);
    });
    clearData('outboxCat');
    // const sentMessages
  } catch (e) {
    console.log(e.message);
  }
};

const sendSpeciesToServer = async () => {
  try {
    const outboxSp = await loadData('outboxSp');
    outboxSp.map((species) => {
      saveSpecies(species);
    });
    clearData('outboxSp');
    // const sentMessages
  } catch (e) {
    console.log(e.message);
  }
};

const sendAnimalToServer = async () => {
  try {
    const outboxAnimal = await loadData('outboxAnimal');
    outboxAnimal.map((animal) => {
      saveAnimal(animal);
    });
    clearData('outboxAnimal');
    // const sentMessages
  } catch (e) {
    console.log(e.message);
  }
};
