const fetchGraphql = async (query) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(query),
  };
  try {
    const response = await fetch('http://localhost:3000/graphql', options);
    const json = await response.json();
    return json.data;
  } catch (e) {
    console.log(e);
    return false;
  }
};

const getAnimals = async () => {
  const query = {
    query: `{
            animals {
              id
              animalName
              species {
                speciesName
                category {
                  categoryName
                }
              }
            }
          }`,
  };
  const data = await fetchGraphql(query);
  return data.animals;
};

const saveCategory = async (category) => {
  console.log('category save', category);
  const mutationCategory = {
    query: `
            mutation VariableTest($categoryName: String!){
              addCategory(categoryName: $categoryName){
                id
                categoryName
              }
            }
            `,
    variables: category,
  };
  const data = await fetchGraphql(mutationCategory);
  console.log('addCategory result:', data.addCategory);
  return data.addCategory;
};

const saveSpecies = async (species) => {
  const mutationSpecies = {
    query: `
            mutation VariableTest($speciesName: String!, $category: ID!){
              addSpecies(speciesName: $speciesName, category: $category){
                id
                speciesName
              }
            }
            `,
    variables: species,
  };
  const data = await fetchGraphql(mutationSpecies);
  console.log('addSpecies result:', data.addSpecies);
  return data.addSpecies;
};

const saveAnimal = async (animal) => {
  const mutationAnimal = {
    query: `
            mutation VariableTest($animalName: String!, $species: ID!){
              addAnimal(animalName: $animalName, species: $species){
                id
                animalName
              }
            }
            `,
    variables: animal,
  };
  const data = await fetchGraphql(mutationAnimal);
  console.log('addAnimal result:', data.addAnimal);
  return data.addAnimal;
};
