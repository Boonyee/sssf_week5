'use strict';

window.addEventListener('load', async () => {
  const ul = document.querySelector('ul');
  const rfrsh = document.querySelector('#refresh');
  const categoryFrom = document.querySelector('.category-form');
  const speciesForm = document.querySelector('.species-form');
  const animalForm = document.querySelector('.animal-form');

  const categoryName = categoryFrom.elements.categoryName;
  const speciesName = speciesForm.elements.speciesName;
  const animalName = animalForm.elements.animalName;

  const categoryID = speciesForm.elements.category;
  const speciesID = animalForm.elements.species;
  console.log('hello');

  if ('serviceWorker' in navigator) {
    try {
      await navigator.serviceWorker.register('./sw.js');
      const registration = await navigator.serviceWorker.ready;
      if ('sync' in registration) {
        /* Add Category */
        categoryFrom.addEventListener('submit', async (event) => {
          event.preventDefault();
          const category = {
            categoryName: categoryName.value,
          };
          try {
            saveData('outboxCat', category);
            await registration.sync.register('send-category');
          } catch (e) {
            console.log(e.message);
          }
        });
        /* Add Species */
        speciesForm.addEventListener('submit', async (event) => {
          event.preventDefault();
          const species = {
            speciesName: speciesName.value,
            category: categoryID.value,
          };
          try {
            saveData('outboxSp', species);
            await registration.sync.register('send-species');
          } catch (e) {
            console.log(e.message);
          }
        });
        /* Add Animal*/
        animalForm.addEventListener('submit', async (event) => {
          event.preventDefault();
          const animal = {
            animalName: animalName.value,
            species: speciesID.value,
          };
          try {
            saveData('outboxAnimal', animal);
            await registration.sync.register('send-animal');
          } catch (e) {
            console.log(e.message);
          }
        });
      }
    } catch (e) {
      console.log(e.message);
    }
  }

  const init = async () => {
    const data = [];
    try {
      // const greetings = await getGreetingsByUser(username);
      const animals = await getAnimals();
      for (const message of animals) {
        data.push(message);
      }
    } catch (e) {
      console.log(e.message);
    }
    ul.innerHTML = '';
    data.forEach((item) => {
      ul.innerHTML += `<ul>${item.id} || ${item.animalName} || ${item.species.speciesName} || ${item.species.category.categoryName}</ul>`;
    });
  };

  init();

  rfrsh.addEventListener('click', init);
});
